package boardgames.test;

import org.junit.Before;
import org.junit.Test;

import boardgames.model.CheckMate;
import boardgames.model.ChessGame;
import boardgames.model.board.BoardImpl;
import boardgames.model.board.Box;
import boardgames.model.board.ChessBoard;
import boardgames.model.piece.Pawn;
import boardgames.model.piece.PieceImpl;
import boardgames.utility.Colour;
import boardgames.utility.PairImpl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Collections;
import java.util.Optional;

/**
 * Test class for Chess methods.
 *
 */
public class ChessLogicTest {
    private ChessGame game;
    private BoardImpl b;

    /**
     * Initialize Chess game.
     */
    @Before
    public void setGame() {
        game = new ChessGame();
        b = new BoardImpl();
        b.setBoardType(new ChessBoard());
        b.reset();
    }

    /**
     * Black and white player at the beginning have 16 pieces each.
     */
    @Test
    public void initialSize() {
        assertEquals(game.getBoard().getBlackPieces().size(), 16, "Assert size is 16");
        assertEquals(game.getBoard().getWhitePieces().size(), 16, "Assert size is 16");
    }

    /**
     * Testing pieces initial possible moves.
     */
    @Test
    public void chessPiecesInitialPossibleMovesTest() {
     // CHECKSTYLE: MagicNumber OFF
        game.getBoard().getWhitePieces().stream().limit(7).forEach(p -> {
            assertEquals(p.possibleMoves(b).size(), 2, "Assert Pawn have 2 moves");
            assertEquals(p.getName(), "Pawn", "Assert name is right");
        });

        assertEquals(game.getBoard().getWhitePieces().get(8).getName(), "King", "Correct name is King");
        assertEquals(game.getBoard().getWhitePieces().get(8).possibleMoves(b), Collections.emptyList(), "King has no moves");

        assertEquals(game.getBoard().getWhitePieces().get(9).getName(), "Queen", "Correct name is Queen");
        assertEquals(game.getBoard().getWhitePieces().get(9).possibleMoves(b), Collections.emptyList(), "Queen has no moves");

        assertEquals(game.getBoard().getWhitePieces().get(10).getName(), "Rook", "Correct name is Rook");
        assertEquals(game.getBoard().getWhitePieces().get(10).possibleMoves(b), Collections.emptyList(), "Rook has no moves");

        assertEquals(game.getBoard().getWhitePieces().get(11).getName(), "Rook", "Correct name is Rook");
        assertEquals(game.getBoard().getWhitePieces().get(11).possibleMoves(b), Collections.emptyList(), "Rook has no moves");

        assertEquals(game.getBoard().getWhitePieces().get(12).getName(), "Bishop", "Correct name is Bishop");
        assertEquals(game.getBoard().getWhitePieces().get(12).possibleMoves(b), Collections.emptyList(), "Bishop has no moves");

        assertEquals(game.getBoard().getWhitePieces().get(13).getName(), "Bishop", "Correct name is Bishop");
        assertEquals(game.getBoard().getWhitePieces().get(13).possibleMoves(b), Collections.emptyList(), "Bishop has no moves");

        assertEquals(game.getBoard().getWhitePieces().get(14).getName(), "Knight", "Correct name is Knight");
        assertNotEquals("Knight has possible moves", game.getBoard().getWhitePieces().get(14).possibleMoves(b), Collections.emptyList());

        assertEquals(game.getBoard().getWhitePieces().get(14).getName(), "Knight", "Correct name is Knight");
        assertNotEquals("Knight has possible moves", game.getBoard().getWhitePieces().get(14).possibleMoves(b), Collections.emptyList());

    }

    /**
     * Test piece possible moves after movement.
     */
    @Test
    public void chessPiecesAfterMovementPossibleMovesTest() {
        assertTrue("Pawn can move in 0,3", game.move(Colour.White, game.getBoard().getWhitePieces().get(0), b.getBox(0, 3)));
        game.updateMove(game.getBoard().getWhitePieces().get(0), b.getBox(0, 3));
        assertEquals(game.getBoard().getWhitePieces().get(0).possibleMoves(b).get(0).getPair(), new PairImpl<>(0, 4), "Pawn can move in 0,4 after move");
        assertFalse("Queen can't move", game.move(Colour.White, game.getBoard().getWhitePieces().get(9), b.getBox(3, 1)));
        assertTrue("Pawn can move in 3,3", game.move(Colour.White, game.getBoard().getWhitePieces().get(3), b.getBox(3, 3)));
        game.updateMove(game.getBoard().getWhitePieces().get(3), b.getBox(3, 3));
        assertEquals(game.getBoard().getWhitePieces().get(3).possibleMoves(b).get(0).getPair(), new PairImpl<>(3, 4), "Pawn can move in 3,4");
        assertTrue("Queen can move after Pawn move", game.move(Colour.White, game.getBoard().getWhitePieces().get(9), b.getBox(3, 1)));
        game.updateMove(game.getBoard().getWhitePieces().get(9), b.getBox(3, 1));

    }

    /**
     * Test if piece promotion happens in right way.
     */
    @Test
    public void testPromotion() {
        final Box b = new Box(new PairImpl<>(7, 7), Optional.empty());
        final PieceImpl p = new PieceImpl(b, Colour.White);
        p.setPieceType(new Pawn(b));
        b.setPiece(Optional.of(p));

        game.setLastPieceMoved(Optional.of(p));
        game.promotion(Colour.White, "Queen");

        assertTrue("Now there is the queen", p.getPieceType().get().getName().equals("Queen"));

    }


    /**
     * Test if Check is correct.
     */
    @Test
    public void simulateCheck() {
        b.reset();
        //simulo una partita per mettere il re bianco sotto scacco
        final CheckMate check = game.getWhiteKingCheckMateController();

        assertTrue("Pawn can move in 3,3", game.move(Colour.White, game.getBoard().getWhitePieces().get(3), game.getBoard().getBox(3, 3)));
        game.updateMove(game.getBoard().getWhitePieces().get(3), game.getBoard().getBox(3, 3));

        assertTrue("Pawn can move in 4,4", game.move(Colour.Black, game.getBoard().getBlackPieces().get(4), game.getBoard().getBox(4, 4)));
        game.updateMove(game.getBoard().getBlackPieces().get(4), game.getBoard().getBox(4, 4));

        assertTrue("Pawn can move in 4,4", game.move(Colour.White, game.getBoard().getWhitePieces().get(7), game.getBoard().getBox(7, 3)));
        game.updateMove(game.getBoard().getWhitePieces().get(4), game.getBoard().getBox(4, 4));

        assertTrue("Pawn can move in 1,3", game.move(Colour.Black, game.getBoard().getBlackPieces().get(13), game.getBoard().getBox(1, 3)));
        game.updateMove(game.getBoard().getBlackPieces().get(13), game.getBoard().getBox(1, 3));

        assertTrue("Must be empty", check.getMustMovements().isEmpty());
        assertFalse("Mustn't be empty", check.getHelper().isEmpty());
        assertFalse("It isn't check mate", check.isCheckMate(game.getLastPieceMoved().get()));

    }

    /**
     * Test if piece is correctly remove from board after it has been eaten.
     */
    @Test
    public void eatTest() {
        b.reset();
        final PieceImpl p = game.getBoard().getWhitePieces().get(3);

        assertTrue("Pawn can move", game.move(Colour.White, p, game.getBoard().getBox(3, 3)));
        game.updateMove(p, game.getBoard().getBox(3, 3));

        assertTrue("Pawn can move", game.move(Colour.Black, game.getBoard().getBlackPieces().get(4), game.getBoard().getBox(4, 4)));
        game.updateMove(game.getBoard().getBlackPieces().get(4), game.getBoard().getBox(4, 4));

        assertTrue("Pawn eats", game.move(Colour.White, game.getBoard().getBox(3, 3).get().getPiece().get(), game.getBoard().getBox(4, 4)));
        game.updateMove(game.getBoard().getWhitePieces().get(3), game.getBoard().getBox(4, 4));

        assertFalse("Pawn is no longer on board", game.getBoard().getBlackPieces().contains(game.getLastPieceEated().get(0)));

    }

    /**
     * Testing exception.
     */
    @Test (expected = IllegalArgumentException.class) 
    public void wrongCoordinates() {
        b.getBox(-1, 0);
        b.getBox(8, 8);
    }

}
