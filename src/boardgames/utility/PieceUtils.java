package boardgames.utility;

import java.util.List;
import boardgames.model.board.BoardImpl;
import boardgames.model.board.Box;

/**
 * This interface defines the utility functions required by part types.
 */
public interface PieceUtils {

    /**
     * @param x the x coordinate of the box
     * @param y the y coordinate of the box
     * @return true if the coordinates indicated are valid, otherwise false.
     */
    boolean checkRange(int x, int y);
 
    /**
     * Removes from moves all the box that aren't valid.
     * @param moves the List of Box that has to be checked
     */
    void checkRangeMoves(List<Box> moves);

    /**
     * @param x the x coordinate of the box
     * @param y the y coordinate of the box
     * @param c the colour of the piece that has to be moved
     * @param b the board on which the piece is moving
     * @return false if in the indicated box there is a piece of the same color, true otherwise
     */
    boolean checkOpponent(int x, int y, Colour c, BoardImpl b);

    /**
     * Eliminates from the possible moves the boxes containing pieces of the same colour of who is moving.
     * @param colour the Colour of the moving piece
     * @param moves the List containing the possible moves of the piece
     * @param b the board on which the piece is moving
     */
    void checkBoxMoves(Colour colour, List<Box> moves, BoardImpl b);
}
