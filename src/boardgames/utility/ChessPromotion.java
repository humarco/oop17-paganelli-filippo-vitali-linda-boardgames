package boardgames.utility;

/**
 * This enumeration defines the type of all chess promotion pieces.
 */
public enum ChessPromotion {

    /**
     * chess pieces upgrade.
     */
    Queen, Bishop, Knight, Rook;
}
