package boardgames.utility;

/**
 * This enumeration defines the type of all checkers promotion pieces.
 */
public enum CheckersPromotion {

    /**
     * checker piece upgrade.
     */
    King;
}
