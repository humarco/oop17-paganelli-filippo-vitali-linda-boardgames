package boardgames.utility;

/**
 * This enumeration defines the type of all chess pieces.
 */
public enum AllChessPieces {

    /**
     * chess pieces.
     */
    King, Queen, Rook, Bishop, Knight, Pawn;
}
