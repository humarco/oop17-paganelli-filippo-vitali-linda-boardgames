package boardgames.view.board;

import java.awt.Color;

import boardgames.controller.GameControllerImpl;

/**
 * This class is used for Checkers board's settings.
 *
 */
public class CheckersBoardView extends BoardViewImpl {

    /**
     * @param ctr controller of entire application
     * @param dim board dimension
     */
    public CheckersBoardView(final GameControllerImpl ctr, final Integer dim) {
        super(ctr, dim);
    }

    @Override
    protected final Color getFirstBoxColor() {
        return new Color(BoardViewImpl.getRgb2());

    }

    @Override
    protected final Color getSecondBoxColor() {
        return new Color(BoardViewImpl.getRgb1());

    }

    @Override
    protected final String nameIconExtension() {
        return "(Checkers).png";
    }

}
