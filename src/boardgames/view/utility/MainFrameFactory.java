package boardgames.view.utility;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Optional;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneLayout;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;

/**
 * Class that implement a factory for the principal frames of the application.
 *
 */
public final class MainFrameFactory implements GameFrameFactory {

    @Override
    public JFrame createFrame(final Optional<String> title) {
        final JFrame jf = new JFrame(title.get());
        jf.setLayout(new BorderLayout());
        jf.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        jf.addWindowListener(new WindowAdapter() {
            public void windowClosing(final WindowEvent e) {
                final int n = JOptionPane.showConfirmDialog(jf, "Do you really want to quit ?", "Quitting ..",
                        JOptionPane.YES_NO_OPTION);
                if (n == JOptionPane.YES_OPTION) {
                    System.exit(0);
                }
            }
        });
        return jf;
    }

    @Override
    public JPanel cratePanel(final Optional<String> title, final LayoutManager lm, final boolean visible) {
        final JPanel panel = new JPanel();
        panel.setLayout(lm);
        if (!title.equals(Optional.empty())) {
            panel.setBorder(new TitledBorder(title.get()));
        }
        panel.setVisible(visible);
        return panel;
    }

    @Override
    public JButton createButton(final Optional<ActionListener> al, final Optional<String> label) {
        final JButton jb = new JButton();
        if (!label.equals(Optional.empty())) {
            jb.setText(label.get());
        }
        if (!al.equals(Optional.empty())) {
            jb.addActionListener(al.get());
        }
        jb.setOpaque(true); // required by Mac systems
        return jb;
    }

    @Override
    public JLabel createJLabel(final Optional<String> s) {
        return new JLabel(s.get());
    }

    @Override
    public GridBagConstraints createGridBagConstraints() {
        final GridBagConstraints cnst = new GridBagConstraints();
        cnst.insets = new Insets(3, 3, 3, 3);
        cnst.gridy = 0;
        return cnst;
    }

    @Override
    public JScrollPane createJScrollPane(final Component c, final ScrollPaneLayout l) {
        final JScrollPane jsp = new JScrollPane(c);
        jsp.setLayout(l);
        return jsp;
    }

    /**
     * @param title
     *            name of text area
     * @param opaque
     *            specify if the background should be visible
     * @param editable
     *            specify if it should be editable
     * @return new JTextArea with specific features
     */
    public static JTextArea createJTextArea(final Optional<String> title, final boolean opaque, final boolean editable) {
        final JTextArea t = new JTextArea();
        if (!title.equals(Optional.empty())) {
            t.setBorder(new TitledBorder(title.get()));
        }
        t.setOpaque(opaque); /* Set to make the background area invisible */
        t.setEditable(editable);
        return t;
    }

   @Override
    public GroupLayout createGroupLayout(final JPanel panel) {
        final GroupLayout cpl = new GroupLayout(panel);
        cpl.setAutoCreateGaps(true);
        cpl.setAutoCreateContainerGaps(true);
        return cpl;

    }

   @Override
    public JCheckBox createCheckBox(final Optional<String> title) {
        return new JCheckBox(title.get());
    }


}
