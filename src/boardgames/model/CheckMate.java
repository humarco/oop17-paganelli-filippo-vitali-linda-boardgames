package boardgames.model;

import java.util.List;
import boardgames.model.board.Box;
import boardgames.model.piece.PieceImpl;
import boardgames.utility.PairImpl;
import boardgames.view.game.GameView;

/**
 * This interface defines the functions useful for checking the status of the king.
 */
public interface CheckMate {

    /**
     * @param lastPieceMoved is the last piece moved which put the king in check
     * @return true if the king is in checkmate or check, otherwise false.
     */
    boolean isCheckMate(PieceImpl lastPieceMoved);

    /**
     * @return the possible moves that the king can do in case of check to save himself, 
     * otherwise an empty list
     */
    List<Box> getMustMovements();

    /**
     * @return the king on which there is checking the check state
     */
    PieceImpl getKing();

    /**
     * @return a List containing the piece and the move that it can do in case of check when 
     * king hasn't possible moves, otherwise an empty List.
     */
    List<PairImpl<PieceImpl, Box>> getHelper();

    /**
     * Inserts a GameView in the observer list.
     * @param gv GameView to be notified in case of an event
     */
    void attach(GameView gv);

    /**
     * It notifies all observers in case of checkmate.
     */
    void notifyCheckMate();

    /**
     * It notifies all observers in case of check.
     */
    void notifyCheck();
}
