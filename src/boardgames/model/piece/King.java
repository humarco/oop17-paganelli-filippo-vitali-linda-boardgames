package boardgames.model.piece;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import boardgames.model.board.BoardImpl;
import boardgames.model.board.Box;
import boardgames.utility.AllChessPieces;
import boardgames.utility.Colour;
import boardgames.utility.PieceUtilsImpl;

/**
 * This class implements the functions defined by PieceType for representing the King's logic.
 */
public class King implements PieceType {
    private final PieceUtilsImpl pu = new PieceUtilsImpl();
    private static final String NAME = "King";

    @Override
    public final List<Box> possibleMoves(final BoardImpl b, final Box current, final Colour myColour) {
        final List<Box> moves = this.calcMoves(b, current, myColour);
        final List<Box> allPossibleMoves = new LinkedList<>();
        List<PieceImpl> opponents;
        final List<Box> effective = new LinkedList<>();

        if (myColour.equals(Colour.Black)) {
            opponents = b.getWhitePieces();
        } else {
            opponents = b.getBlackPieces();
        }
        opponents.forEach(piece -> {
            if (piece.getName().equals(AllChessPieces.King.toString())) {
                final King king = (King) piece.getPieceType().get();
                allPossibleMoves.addAll(king.calcMoves(b, piece.getBox().get(), piece.getColour()));
            } else {
                allPossibleMoves.addAll(piece.possibleMoves(b));
            }
        });
        for (final Box kingMoves : moves) {
            final PieceImpl pieceKing = current.getPiece().get();
            if (!kingMoves.isEmpty()) {
                // simulo mangiata
                final PieceImpl pieceEated = kingMoves.getPiece().get();
                pieceKing.setBox(Optional.of(kingMoves));
                kingMoves.setPiece(Optional.of(pieceKing));
                current.setPiece(Optional.empty());
                pieceEated.setBox(Optional.empty());
                if (pieceEated.getColour().equals(Colour.Black)) {
                    opponents = b.getBlackPieces();
                } else {
                    opponents = b.getWhitePieces();
                }
                allPossibleMoves.clear();
                opponents.forEach(piece -> {
                    if (piece.getName().equals(AllChessPieces.King.toString())) {
                        final King king = (King) piece.getPieceType().get();
                        allPossibleMoves.addAll(king.calcMoves(b, piece.getBox().get(), piece.getColour()));
                    } else {
                        if (!piece.equals(pieceEated)) {
                            allPossibleMoves.addAll(piece.possibleMoves(b));
                        }
                    }
                });
                if (!allPossibleMoves.contains(kingMoves)) {
                    effective.add(kingMoves);
                }
                pieceKing.setBox(Optional.of(current));
                current.setPiece(Optional.of(pieceKing));
                kingMoves.setPiece(Optional.of(pieceEated));
                pieceEated.setBox(Optional.of(kingMoves));
            } else {
             // simulo mossa
                pieceKing.setBox(Optional.of(kingMoves));
                kingMoves.setPiece(Optional.of(pieceKing));
                current.setPiece(Optional.empty());
                if (myColour.equals(Colour.Black)) {
                    opponents = b.getWhitePieces();
                } else {
                    opponents = b.getBlackPieces();
                }
                allPossibleMoves.clear();
                opponents.forEach(piece -> {
                    if (piece.getName().equals(AllChessPieces.King.toString())) {
                        final King king = (King) piece.getPieceType().get();
                        allPossibleMoves.addAll(king.calcMoves(b, piece.getBox().get(), piece.getColour()));
                    } else {
                        if (piece.getBox().isPresent()) {
                            allPossibleMoves.addAll(piece.possibleMoves(b));
                        }
                    }
                });
                if (!allPossibleMoves.contains(kingMoves)) {
                    effective.add(kingMoves);
                }
                pieceKing.setBox(Optional.of(current));
                current.setPiece(Optional.of(pieceKing));
                kingMoves.setPiece(Optional.empty());
            }
        }
        return effective;
    }

    /**
     * 
     * @param b the Board where the King moves
     * @param current a Box indicating the current position of the King
     * @param colour the Colour of the King
     * @return a List of Box containing the possible moves of King without check if it can be eated
     */
    public final List<Box> calcMoves(final BoardImpl b, final Box current, final Colour colour) {
        final int x = current.getX();
        final int y = current.getY();
        final List<Box> moves = new LinkedList<>();

        if (this.pu.checkRange(x + 1, y) && this.pu.checkOpponent(x + 1, y, colour, b)) {
            moves.add(b.getBox(x + 1, y).get());
        }
        if (this.pu.checkRange(x, y + 1) && this.pu.checkOpponent(x, y + 1, colour, b)) {
            moves.add(b.getBox(x, y + 1).get());
        }
        if (this.pu.checkRange(x - 1, y) && this.pu.checkOpponent(x - 1, y, colour, b)) {
            moves.add(b.getBox(x - 1, y).get());
        }
        if (this.pu.checkRange(x, y - 1) && this.pu.checkOpponent(x, y - 1, colour, b)) {
            moves.add(b.getBox(x, y - 1).get());
        }
        if (this.pu.checkRange(x + 1, y + 1) && this.pu.checkOpponent(x + 1, y + 1, colour, b)) {
            moves.add(b.getBox(x + 1, y + 1).get());
        }
        if (this.pu.checkRange(x - 1, y - 1) && this.pu.checkOpponent(x - 1, y - 1, colour, b)) {
            moves.add(b.getBox(x - 1, y - 1).get());
        }
        if (this.pu.checkRange(x + 1, y - 1) && this.pu.checkOpponent(x + 1, y - 1, colour, b)) {
            moves.add(b.getBox(x + 1, y - 1).get());
        }
        if (this.pu.checkRange(x - 1, y + 1) && this.pu.checkOpponent(x - 1, y + 1, colour, b)) {
            moves.add(b.getBox(x - 1, y + 1).get());
        }
        return moves;
    }

    @Override
    public final String getName() {
        return NAME;
    }
}
