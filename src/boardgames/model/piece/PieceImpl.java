package boardgames.model.piece;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import boardgames.model.board.BoardImpl;
import boardgames.model.board.Box;
import boardgames.utility.Colour;

/**
 * This class implements the common model for the pieces.
 */
public class PieceImpl implements Piece {

    private Optional<Box> piecePosition;
    private final Optional<Colour> pieceColour;
    private Optional<PieceType> type;
    private static final Logger LOGGER = LogManager.getLogger(PieceImpl.class.getName());

    /**
     * @param piecePosition the Box where the Piece is initially located
     * @param pieceColour the colour of the Piece
     */
    public PieceImpl(final Box piecePosition, final Colour pieceColour) {
        this.type = Optional.empty();
        this.piecePosition = Optional.of(piecePosition);
        this.pieceColour = Optional.of(pieceColour);
    }

    @Override
    public final void setPieceType(final PieceType type) {
        this.type = Optional.of(type);
    }

    @Override
    public final Optional<PieceType> getPieceType() {
        return this.type;
    }

    @Override
    public final String toString() {
        if (!piecePosition.isPresent()) {
            LOGGER.debug("Box empty setted");
            throw new NoSuchElementException();
        } else {
            return piecePosition.get().toString() + ", Colour = " + pieceColour.get();
        }
    }

    @Override
    public final Optional<Box> getBox() {
            return this.piecePosition;
    }

    @Override
    public final void setBox(final Optional<Box> piecePosition) {
        this.piecePosition = piecePosition;
    }

    @Override
    public final Colour getColour() {
        return this.pieceColour.get();
    }

    @Override
    public final String getName() {
        if (!this.type.isPresent()) {
            LOGGER.debug("Piece type not setted");
            throw new IllegalStateException();
        } else {
            return this.type.get().getName();
        }
    }

    @Override
    public final List<Box> possibleMoves(final BoardImpl b) {
        if (!this.type.isPresent()) {
            LOGGER.debug("Piece type not setted");
            throw new IllegalStateException();
        } else {
            return this.type.get().possibleMoves(b, this.getBox().get(), this.getColour());
        }
    }
}
