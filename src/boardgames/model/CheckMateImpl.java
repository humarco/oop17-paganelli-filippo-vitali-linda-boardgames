package boardgames.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import boardgames.model.board.BoardImpl;
import boardgames.model.board.Box;
import boardgames.model.piece.PieceImpl;
import boardgames.utility.AllChessPieces;
import boardgames.utility.Colour;
import boardgames.utility.PairImpl;
import boardgames.view.game.GameView;

/**
 * This class implements the functions useful to check the status of the King.
 */
public class CheckMateImpl implements CheckMate {

    private final BoardImpl board;
    private final ChessGame cg;
    private final PieceImpl king;
    private final Player player;
    private final Player opponents;
    private final List<PairImpl<PieceImpl, Box>> helper;
    private final List<GameView> observers;
    private final List<Box> liberty;
    private static final Logger LOGGER = LogManager.getLogger(CheckMateImpl.class.getName());

    /**
     * Sets the useful values to check the status of the King.
     * @param cg the running Game
     * @param king the King whose state is controlled
     * @param player the Player who is the owner of the indicated King
     */
    public CheckMateImpl(final GameImpl cg, final PieceImpl king, final Player player) {
        this.cg = (ChessGame) cg;
        this.board = cg.getBoard();
        this.king = king;
        this.player = player;
        this.opponents = this.getOpponents(player);
        this.helper = new LinkedList<>();
        this.observers = new LinkedList<>();
        this.liberty = new LinkedList<>();
    }

    @Override
    public final List<Box> getMustMovements() {
        return this.liberty;
    }

    @Override
    public final PieceImpl getKing() {
        return this.king;
    }

    @Override
    public final boolean isCheckMate(final PieceImpl lastPieceMoved) {
        this.liberty.clear();
        final List<Box> allPossibleMoves = new LinkedList<>();
        final List<Box> kingPM = this.king.possibleMoves(board);
        Box current;
        this.helper.clear();
        //controllo se il pezzo appena mosso può mangiare il re
        if (lastPieceMoved.possibleMoves(board).contains(this.king.getBox().get())) {
            // inserisco in allMovesPossible tutte le mosse possibili dei pezzi dell'avversario
            this.opponents.getPlayerPieces().forEach(a -> allPossibleMoves.addAll(a.possibleMoves(board)));
            // se tutte le mosse del re dove non mangia (isEmpty) e dove mangia sono contenute in allPossibleMoves 
            // significa che il re è in scacco matto
            for (final Box c : kingPM) {
                // se il box contiene un pezzo che il re può mangiare
                if (!c.isEmpty()) {
                    // simulazione del re che mangia uno dei possibili pezzi mangiabili
                    final PieceImpl temp = c.getPiece().get();
                    temp.setBox(Optional.empty());
                    current = this.king.getBox().get();
                    current.setPiece(Optional.empty());
                    this.king.setBox(Optional.of(c));
                    c.setPiece(Optional.of(this.king));
                    // ricalcolo le possibili mosse
                    this.opponents.getPlayerPieces().forEach(piece -> {
                        if (!piece.equals(temp)) {
                            allPossibleMoves.addAll(piece.possibleMoves(board));
                        }
                    });
                    // se il box in cui il re ha mangiato non è contenuto nella lista allora significa che può muoversi 
                    // li per uscire dallo scacco
                    if (!allPossibleMoves.contains(c)) {
                        this.liberty.add(c);
                    }
                    // ritorno alla situazione iniziale
                    c.setPiece(Optional.of(temp));
                    temp.setBox(Optional.of(c));
                    this.king.setBox(Optional.of(current));
                    current.setPiece(Optional.of(this.king));
                } else {
                    // simulo della mossa del re in un box vuoto
                    current = this.king.getBox().get();
                    current.setPiece(Optional.empty());
                    this.king.setBox(Optional.of(c));
                    c.setPiece(Optional.of(this.king));
                    // ricalcolo le possibili mosse
                    this.opponents.getPlayerPieces().forEach(a -> allPossibleMoves.addAll(a.possibleMoves(board)));
                    // se il box in cui il re si è mosso non è contenuto nella lista allora significa che può muoversi 
                    // li per uscire dallo scacco
                    if (!allPossibleMoves.contains(c)) {
                        this.liberty.add(c);
                    }
                    // rimetto a posto il re
                    this.king.setBox(Optional.of(current));
                    c.setPiece(Optional.empty());
                    current.setPiece(Optional.of(this.king));
                }
            } 
            // se liberty è vuota significa che è scacco matto
            if (liberty.isEmpty()) {
                // se un pezzo dello stesso colore del re può muoversi per togliere il re dallo scacco matto,
                // allora viene segnalato solo lo scacco
                if (this.helpCall()) {
                    LOGGER.debug(this.king.getColour() + this.king.getName() + " check");
                    this.helper.forEach(a -> this.cg.notifyConsole("Can move " + a.getX().getName() + " in " + a.getY()));
                    this.notifyCheck();
                    return false;
                } else {
                    LOGGER.debug(this.king.getColour() + this.king.getName() + " check mate");
                    this.notifyCheckMate();
                    return true;
                }
            } else {
                // liberty contiene qualche mossa quindi è scacco
                LOGGER.debug(this.king.getColour() + this.king.getName() + " check");
                this.notifyCheck();
                return false;
            }
        }
        this.liberty.clear();
        return false;
    }

    private boolean helpCall() {
        final List<Box> allPossibleMoves = new LinkedList<>();
        final List<PieceImpl> myPieces = new LinkedList<>(this.player.getPlayerPieces());
        final Box kingPos = this.king.getBox().get();
        final List<Box> pieceMoves = new LinkedList<>();
        boolean canHelp = false;

        for (final PieceImpl piece : myPieces) {
            pieceMoves.clear();
            if (!piece.getName().equals(AllChessPieces.King.toString())) {
                pieceMoves.addAll(piece.possibleMoves(board));
            }
            final Box current = piece.getBox().get();
            PieceImpl temp;
            for (final Box box : pieceMoves) {
                if (!box.isEmpty()) {
                    // simulo mangiata
                    temp = box.getPiece().get();
                    temp.setBox(Optional.empty());
                    piece.setBox(Optional.of(box));
                    box.setPiece(Optional.of(piece));
                    current.setPiece(Optional.empty());
                    this.opponents.removePiece(temp);
                    // calcolo mosse possibili dell avversario
                    this.opponents.getPlayerPieces().forEach(a -> {
                        if (a.getBox().isPresent()) {
                            allPossibleMoves.addAll(a.possibleMoves(board));
                        }
                    });
                    // se la combinazione di mosse contiene il re significa che non può essere aiutato dalla
                    // mossa effettuata dal pezzo amico
                    if (!allPossibleMoves.contains(kingPos)) {
                        this.helper.add(new PairImpl<>(piece, box));
                        LOGGER.debug("Piece that can help king: " + piece.getName() + " in Box: " + box.toString());
                        canHelp = true;
                    }
                    this.opponents.addPiece(temp);
                    allPossibleMoves.clear();
                    current.setPiece(Optional.of(piece));
                    piece.setBox(Optional.of(current));
                    temp.setBox(Optional.of(box));
                    box.setPiece(Optional.of(temp));
                    // necessario per reimpostare il pedone nella situazione di prima mossa in caso non sia stato ancora mosso
                    if (piece.getName().equals(AllChessPieces.Pawn.toString())) {
                        piece.possibleMoves(board);
                    }
                } else {
                    // simulo mossa
                    box.setPiece(Optional.of(piece));
                    piece.setBox(Optional.of(box));
                    current.setPiece(Optional.empty());
                    this.opponents.getPlayerPieces().forEach(d -> allPossibleMoves.addAll(d.possibleMoves(board)));
                    if (!allPossibleMoves.contains(kingPos)) {
                        this.helper.add(new PairImpl<>(piece, box));
                        LOGGER.debug("Piece that can help king: " + piece.getName() + " in Box: " + box.toString());
                        canHelp = true;
                    }
                    allPossibleMoves.clear();
                    box.setPiece(Optional.empty());
                    piece.setBox(Optional.of(current));
                    current.setPiece(Optional.of(piece));
                    // necessario per reimpostare il pedone nella situazione di prima mossa in caso non sia stato ancora mosso
                    if (piece.getName().equals(AllChessPieces.Pawn.toString())) {
                        piece.possibleMoves(board);
                    }
                }
            }
        }
        return canHelp;
    }

    @Override
    public final List<PairImpl<PieceImpl, Box>> getHelper() {
        return this.helper;
    }

    private Player getOpponents(final Player player) {
        if (player.getColour() == Colour.White) {
            return cg.getPlayer(Colour.Black);
        } else {
            return cg.getPlayer(Colour.White);
        }
    }

    @Override
    public final void attach(final GameView gv) {
        this.observers.add(gv);
    }

    @Override
    public final void notifyCheckMate() {
       this.observers.forEach(a -> a.updateWinStatus());
    }

    @Override
    public final void notifyCheck() {
        this.observers.forEach(a -> a.updatePieceStatus());
    }
}
