package boardgames.model;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import boardgames.model.board.BoardImpl;
import boardgames.model.piece.PieceImpl;
import boardgames.utility.Colour;

/**
 * This class implements the functions useful for modeling the player.
 */
public class PlayerImpl implements Player {

    private final List<PieceImpl> playerPieces;
    private final Colour player;
    private static final Logger LOGGER = LogManager.getLogger(PlayerImpl.class.getName());

    /**
     * @param player the Colour of the Player
     * @param board the Board used in the game
     */
    public PlayerImpl(final Colour player, final BoardImpl board) {
        this.player = player;
        if (player == Colour.Black) {
            this.playerPieces = board.getBlackPieces();
        } else {
            this.playerPieces = board.getWhitePieces();
        }
    }

    @Override
    public final List<PieceImpl> getPlayerPieces() {
        return this.playerPieces;
    }

    @Override
    public final void removePiece(final PieceImpl p) {
        if (!this.playerPieces.remove(p)) {
            LOGGER.debug(player.toString() + " player not contains " + p.getName());
        }
    }

    @Override
    public final Colour getColour() {
        return this.player;
    }

    @Override
    public final void addPiece(final PieceImpl p) {
        if (p.getColour().equals(player)) {
            this.playerPieces.add(p);
        } else {
            LOGGER.debug(player.toString() + " player can't has a " + p.getColour().toString() + " piece");
        }
    }
}
