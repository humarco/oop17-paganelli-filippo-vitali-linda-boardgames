package boardgames.model.board;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import boardgames.model.piece.PieceImpl;
import boardgames.model.piece.Bishop;
import boardgames.model.piece.King;
import boardgames.model.piece.Knight;
import boardgames.model.piece.Pawn;
import boardgames.model.piece.Queen;
import boardgames.model.piece.Rook;
import boardgames.utility.Colour;
import boardgames.utility.PairImpl;
import boardgames.utility.PieceUtilsImpl;

/**
 * This class implements the functions useful for modeling the Chessboard.
 */
public class ChessBoard implements BoardType {
 
    private static final int PLAYER_PAWNS = 8;
    private static final int PLAYER_WHITE_PAWNS_ROW = 1;
    private static final int PLAYER_BLACK_PAWNS_ROW = 6;
    private static final int WHITE_KING_X = 4;
    private static final int WHITE_KING_Y = 0;
    private static final int BLACK_KING_X = 4;
    private static final int BLACK_KING_Y = 7;
    private static final int WHITE_QUEEN_X = 3;
    private static final int WHITE_QUEEN_Y = 0;
    private static final int BLACK_QUEEN_X = 3;
    private static final int BLACK_QUEEN_Y = 7;
    private static final int WHITE_1ROOK_X = 0;
    private static final int WHITE_1ROOK_Y = 0;
    private static final int WHITE_2ROOK_X = 7;
    private static final int WHITE_2ROOK_Y = 0;
    private static final int BLACK_1ROOK_X = 0;
    private static final int BLACK_1ROOK_Y = 7;
    private static final int BLACK_2ROOK_X = 7;
    private static final int BLACK_2ROOK_Y = 7;
    private static final int WHITE_1BISHOP_X = 2;
    private static final int WHITE_1BISHOP_Y = 0;
    private static final int WHITE_2BISHOP_X = 5;
    private static final int WHITE_2BISHOP_Y = 0;
    private static final int BLACK_1BISHOP_X = 2;
    private static final int BLACK_1BISHOP_Y = 7;
    private static final int BLACK_2BISHOP_X = 5;
    private static final int BLACK_2BISHOP_Y = 7;
    private static final int WHITE_1KNIGHT_X = 1;
    private static final int WHITE_1KNIGHT_Y = 0;
    private static final int WHITE_2KNIGHT_X = 6;
    private static final int WHITE_2KNIGHT_Y = 0;
    private static final int BLACK_1KNIGHT_X = 1;
    private static final int BLACK_1KNIGHT_Y = 7;
    private static final int BLACK_2KNIGHT_X = 6;
    private static final int BLACK_2KNIGHT_Y = 7;
    private final List<PieceImpl> whitePieces;
    private final List<PieceImpl> blackPieces;
    private final List<List<Box>> allPiecesOnBoard;
    private PieceImpl whiteKing;
    private PieceImpl blackKing;

    /**
     * Initializes the structures useful for the board and the pieces that use it.
     */
    public ChessBoard() {
        this.whitePieces = new LinkedList<>();
        this.blackPieces = new LinkedList<>();
        this.allPiecesOnBoard = new LinkedList<>();
        for (int i = 0; i <= PieceUtilsImpl.BOARD_SIZE; i++) {
            this.allPiecesOnBoard.add(new LinkedList<Box>());
            for (int y = 0; y <= PieceUtilsImpl.BOARD_SIZE; y++) {
                this.allPiecesOnBoard.get(i).add(new Box(new PairImpl<>(i, y), Optional.empty()));
            }
        }
    }

    @Override
    public final List<List<Box>> reset() {
       for (int i = 0; i < PLAYER_PAWNS; i++) {
           /*   Initialization of white player's pawns
            */
           final PieceImpl pW = new PieceImpl(allPiecesOnBoard.get(i).get(PLAYER_WHITE_PAWNS_ROW), Colour.White);
           pW.setPieceType(new Pawn(allPiecesOnBoard.get(i).get(PLAYER_WHITE_PAWNS_ROW)));
           this.addWhitePiece(pW);
           allPiecesOnBoard.get(i).get(PLAYER_WHITE_PAWNS_ROW).setPiece(Optional.of(pW));
           /*   Initialization of black player's pawns
            */
           final PieceImpl pB = new PieceImpl(allPiecesOnBoard.get(i).get(PLAYER_BLACK_PAWNS_ROW), Colour.Black);
           pB.setPieceType(new Pawn(allPiecesOnBoard.get(i).get(PLAYER_BLACK_PAWNS_ROW)));
           this.addBlackPiece(pB);
           allPiecesOnBoard.get(i).get(PLAYER_BLACK_PAWNS_ROW).setPiece(Optional.of(pB));
       }

       final PieceImpl kW = new PieceImpl(allPiecesOnBoard.get(WHITE_KING_X).get(WHITE_KING_Y), Colour.White);
       kW.setPieceType(new King());
       this.whiteKing = kW;
       this.addWhitePiece(kW);
       allPiecesOnBoard.get(WHITE_KING_X).get(WHITE_KING_Y).setPiece(Optional.of(kW));

       final PieceImpl kB = new PieceImpl(allPiecesOnBoard.get(BLACK_KING_X).get(BLACK_KING_Y), Colour.Black);
       kB.setPieceType(new King());
       this.blackKing = kB;
       this.addBlackPiece(kB);
       allPiecesOnBoard.get(BLACK_KING_X).get(BLACK_KING_Y).setPiece(Optional.of(kB));

       final PieceImpl qW = new PieceImpl(allPiecesOnBoard.get(WHITE_QUEEN_X).get(WHITE_QUEEN_Y), Colour.White);
       qW.setPieceType(new Queen());
       this.addWhitePiece(qW);
       allPiecesOnBoard.get(WHITE_QUEEN_X).get(WHITE_QUEEN_Y).setPiece(Optional.of(qW));

       final PieceImpl qB = new PieceImpl(allPiecesOnBoard.get(BLACK_QUEEN_X).get(BLACK_QUEEN_Y), Colour.Black);
       qB.setPieceType(new Queen());
       this.addBlackPiece(qB);
       allPiecesOnBoard.get(BLACK_QUEEN_X).get(BLACK_QUEEN_Y).setPiece(Optional.of(qB));

       final PieceImpl r1W = new PieceImpl(allPiecesOnBoard.get(WHITE_1ROOK_X).get(WHITE_1ROOK_Y), Colour.White);
       r1W.setPieceType(new Rook());
       this.addWhitePiece(r1W);
       allPiecesOnBoard.get(WHITE_1ROOK_X).get(WHITE_1ROOK_Y).setPiece(Optional.of(r1W));

       final PieceImpl r2W = new PieceImpl(allPiecesOnBoard.get(WHITE_2ROOK_X).get(WHITE_2ROOK_Y), Colour.White);
       r2W.setPieceType(new Rook());
       this.addWhitePiece(r2W);
       allPiecesOnBoard.get(WHITE_2ROOK_X).get(WHITE_2ROOK_Y).setPiece(Optional.of(r2W));

       final PieceImpl r1B = new PieceImpl(allPiecesOnBoard.get(BLACK_1ROOK_X).get(BLACK_1ROOK_Y), Colour.Black);
       r1B.setPieceType(new Rook());
       this.addBlackPiece(r1B);
       allPiecesOnBoard.get(BLACK_1ROOK_X).get(BLACK_1ROOK_Y).setPiece(Optional.of(r1B));

       final PieceImpl r2B = new PieceImpl(allPiecesOnBoard.get(BLACK_2ROOK_X).get(BLACK_2ROOK_Y), Colour.Black);
       r2B.setPieceType(new Rook());
       this.addBlackPiece(r2B);
       allPiecesOnBoard.get(BLACK_2ROOK_X).get(BLACK_2ROOK_Y).setPiece(Optional.of(r2B));

       final PieceImpl b1W = new PieceImpl(allPiecesOnBoard.get(WHITE_1BISHOP_X).get(WHITE_1BISHOP_Y), Colour.White);
       b1W.setPieceType(new Bishop());
       this.addWhitePiece(b1W);
       allPiecesOnBoard.get(WHITE_1BISHOP_X).get(WHITE_1BISHOP_Y).setPiece(Optional.of(b1W));

       final PieceImpl b2W = new PieceImpl(allPiecesOnBoard.get(WHITE_2BISHOP_X).get(WHITE_2BISHOP_Y), Colour.White);
       b2W.setPieceType(new Bishop());
       this.addWhitePiece(b2W);
       allPiecesOnBoard.get(WHITE_2BISHOP_X).get(WHITE_2BISHOP_Y).setPiece(Optional.of(b2W));

       final PieceImpl b1B = new PieceImpl(allPiecesOnBoard.get(BLACK_1BISHOP_X).get(BLACK_1BISHOP_Y), Colour.Black);
       b1B.setPieceType(new Bishop());
       this.addBlackPiece(b1B);
       allPiecesOnBoard.get(BLACK_1BISHOP_X).get(BLACK_1BISHOP_Y).setPiece(Optional.of(b1B));

       final PieceImpl b2B = new PieceImpl(allPiecesOnBoard.get(BLACK_2BISHOP_X).get(BLACK_2BISHOP_Y), Colour.Black);
       b2B.setPieceType(new Bishop());
       this.addBlackPiece(b2B);
       allPiecesOnBoard.get(BLACK_2BISHOP_X).get(BLACK_2BISHOP_Y).setPiece(Optional.of(b2B));

       final PieceImpl k1W = new PieceImpl(allPiecesOnBoard.get(WHITE_1KNIGHT_X).get(WHITE_1KNIGHT_Y), Colour.White);
       k1W.setPieceType(new Knight());
       this.addWhitePiece(k1W);
       allPiecesOnBoard.get(WHITE_1KNIGHT_X).get(WHITE_1KNIGHT_Y).setPiece(Optional.of(k1W));

       final PieceImpl k2W = new PieceImpl(allPiecesOnBoard.get(WHITE_2KNIGHT_X).get(WHITE_2KNIGHT_Y), Colour.White);
       k2W.setPieceType(new Knight());
       this.addWhitePiece(k2W);
       allPiecesOnBoard.get(WHITE_2KNIGHT_X).get(WHITE_2KNIGHT_Y).setPiece(Optional.of(k2W));

       final PieceImpl k1B = new PieceImpl(allPiecesOnBoard.get(BLACK_1KNIGHT_X).get(BLACK_1KNIGHT_Y), Colour.Black);
       k1B.setPieceType(new Knight());
       this.addBlackPiece(k1B);
       allPiecesOnBoard.get(BLACK_1KNIGHT_X).get(BLACK_1KNIGHT_Y).setPiece(Optional.of(k1B));

       final PieceImpl k2B = new PieceImpl(allPiecesOnBoard.get(BLACK_2KNIGHT_X).get(BLACK_2KNIGHT_Y), Colour.Black);
       k2B.setPieceType(new Knight());
       this.addBlackPiece(k2B);
       allPiecesOnBoard.get(BLACK_2KNIGHT_X).get(BLACK_2KNIGHT_Y).setPiece(Optional.of(k2B));

       return this.allPiecesOnBoard;
    }

    /**
     * @return a PieceImpl that is the White King
     */
    public final PieceImpl getWhiteKing() {
        return this.whiteKing;
    }

    /**
     * @return a PieceImpl that is the Black King
     */
    public final PieceImpl getBlackKing() {
        return this.blackKing;
    }

    private void addWhitePiece(final PieceImpl pw) {
        this.whitePieces.add(pw);
    }

    private void addBlackPiece(final PieceImpl pb) {
        this.blackPieces.add(pb);
    }

    @Override
    public final List<PieceImpl> getWhiteStartPieces() {
        return this.whitePieces;
    }

    @Override
    public final List<PieceImpl> getBlackStartPieces() {
        return this.blackPieces;
    }
}
